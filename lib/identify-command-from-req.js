const isSessionReq = require('./is-session-req');

// TODO: Other trigger criteria
// see https://support.saucelabs.com/hc/en-us/articles/225265388-Selenium-Commands-that-Trigger-Screenshots


function isEndingSession(req) {
  return {
    type: 'session-end',
    result: req.method === 'DELETE' && isSessionReq(req) && (/session\/.+?\/?$/).test(req.path),
  };
}

function isOpeningUrl(req) {
  return {
    type: 'url',
    result: req.method === 'POST' && isSessionReq(req) && (/url\/?$/).test(req.path),
  };
}

function isClicking(req) {
  return {
    type: 'click',
    result: req.method === 'POST' && isSessionReq(req) && (/element\/.+?\/click\/?$/).test(req.path),
  };
}

function isSendingKeys(req) {
  return {
    type: 'keys',
    result: req.method === 'POST' && isSessionReq(req) && (/element\/.+?\/value\/?$/).test(req.path),
  };
}

function isExecutingJavascript(req) {
  return {
    type: 'executing-javascript',
    result: req.method === 'POST' && isSessionReq(req) && (/execute\/a?sync\/?$/).test(req.path),
  };
}

function isFindingElement(req) {
  return {
    type: 'element',
    result: req.method === 'POST' && isSessionReq(req) && (/elements?\/?$/).test(req.path),
  };
}

module.exports = {
  isEndingSession,
  isOpeningUrl,
  isClicking,
  isSendingKeys,
  isExecutingJavascript,
  isFindingElement
};
